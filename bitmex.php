<?php
/**
 * Created by PhpStorm.
 * User: dgarkaev
 * Date: 27.01.19
 * Time: 23:45
 */

///////////////////////////////////////////////////////////////////////////////
class bitmex extends \ccxt\bitmex
{
    public function __construct($options = array())
    {
        parent::__construct(array_merge(array('i' => 1), $options));
    }

    //walletbalance
    public function fetch_balance ($params = array ()) {
//        $this->load_markets();
        $response = $this->privateGetUserMargin (array ( 'currency' => 'all' ));
        $result = array ( 'info' => $response );
        for ($b = 0; $b < count ($response); $b++) {
            $balance = $response[$b];
            $currency = strtoupper ($balance['currency']);
            $currency = $this->common_currency_code($currency);
            $account = array (
                'free' => $balance['availableMargin'],
                'used' => 0.0,
                'total' => $balance['walletBalance'],
            );
            if ($currency === 'BTC') {
                $account['free'] = $account['free'] * 0.00000001;
                $account['total'] = $account['total'] * 0.00000001;
            }
            $account['used'] = $account['total'] - $account['free'];
            $result[$currency] = $account;
        }
        return $this->parse_balance($result);
    }
}
