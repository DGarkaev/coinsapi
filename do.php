<?php
//$mks = 1000000;
//usleep ( 1*$mks );
date_default_timezone_set('UTC');
$start = microtime(true);

$GNONCE = $start;
$GQ1=0;
$GQ2=0;

require_once 'vendor/autoload.php';
require_once 'db.php';
require_once 'exmo.php';
require_once 'binance.php';
require_once 'dsx.php';
require_once 'bitmex.php';
require_once 'yobit.php';


$dev = file_exists("dev");

///////////////////////////////////////////////////////////////////////////////
function je($d)
{
    return json_encode($d,
        JSON_PRETTY_PRINT |
        JSON_UNESCAPED_SLASHES |
        JSON_NUMERIC_CHECK |
        JSON_UNESCAPED_UNICODE);
}

///////////////////////////////////////////////////////////////////////////////
function getExch()
{
    $param = getKey();
    $e = $_GET['exch'];
    $exch = new $e($param);
    return $exch;
}

///////////////////////////////////////////////////////////////////////////////
function getKey()
{
    $mt = microtime(true);
    $db = getdb();
    $user_id = $_GET['user_id'];
    $exch = $_GET['exch'];
    $sql = "select * from `keys` where exch = '$exch' and account_id = '$user_id' order by dt asc limit 1";
    $rz = $db->query($sql);
    $ks = $rz->fetch_array(MYSQLI_ASSOC);

    $id_key = $ks['id_key'];
    $sql = "UPDATE `keys` SET dt = '$mt' WHERE id_key = '$id_key' and exch = '$exch' and account_id = '$user_id'";
    $rz = $db->query($sql);
    $db->close();

    return array(
        'apiKey' => $ks['key'],
        'secret' => $ks['secret']
    );
}

///////////////////////////////////////////////////////////////////////////////
/// BEGIN
///
header('Content-Type: application/json;charset=utf-8');

if ($dev) {
    //------------------------------------------------------
    //              ORDERBOOK
    //------------------------------------------------------
    $sss = 'exch=dsx&fn=orderbook&user_id=1&sname=boriska&from=btc&to=rub';
    $sss = 'exch=exmo&fn=orderbook&user_id=1&sname=boriska&from=btc&to=rub';
    $sss = 'exch=yobit&fn=orderbook&user_id=1&sname=boriska&from=btc&to=rub';
    //------------------------------------------------------
    //              BALANCE
    //------------------------------------------------------
//    $sss = 'exch=dsx&fn=balance&user_id=1&sname=boriska';
    $sss = 'exch=exmo&fn=balance&user_id=15&sname=boriska';
//    $sss = 'exch=yobit&fn=balance&user_id=1&sname=boriska';
    //------------------------------------------------------
    //              OPENORDERS
    //------------------------------------------------------
//    $sss = 'exch=dsx&fn=openorders&user_id=1&sname=boriska&from=btc&to=rub';
//    $sss = 'exch=exmo&fn=openorders&user_id=1&sname=boriska&from=btc&to=rub';
//    $sss = 'exch=yobit&fn=openorders&user_id=1&sname=boriska&from=btc&to=rub';
    //------------------------------------------------------
    //              ORDERSTATUS
    //------------------------------------------------------
//    $sss = 'exch=dsx&fn=orderstatus&user_id=1&sname=boriska&from=btc&to=rub&orderid=345259484';
//    $sss = 'exch=exmo&fn=orderstatus&user_id=2&sname=boriska&from=btc&to=rub&orderid=1816638449';
//    $sss = 'exch=yobit&fn=orderstatus&user_id=1&sname=boriska&from=btc&to=rub&orderid=2500031487420989';
    //------------------------------------------------------
    //              TRADES
    //------------------------------------------------------
//    $sss = 'exch=dsx&fn=trades&user_id=1&sname=boriska&from=btc&to=rub';
//    $sss = 'exch=exmo&fn=trades&user_id=2&sname=boriska&from=btc&to=rub';
//    $sss = 'exch=yobit&fn=trades&user_id=1&sname=boriska&from=btc&to=rub';

    //-------------------------------------------------------
    //                  TEST
//    $sss = 'exch=exmo&fn=neworder&user_id=1&sname=boriska&from=btc&to=rub&ordertype=market&type=sell&price=290016.655&amount=290';
//    $sss = 'exch=exmo&fn=ordertrades&orderid=1650938989&user_id=3&sname=boriska&from=btc&to=rub';
//    $sss = 'exch=binance&fn=ordertrades&orderid=228838748&user_id=1&sname=boriska&from=btc&to=rub';
//    $sss = "exch=binance&fn=orderbook&from=btc&to=usd&user_id=3&sname=boriska";

//    $sss="exch=dsx&fn=fetchticker&from=btc&to=rub";
//    $sss = 'exch=yobit&fn=balance&user_id=1&sname=test';
//    $sss = 'exch=bitmex&fn=balance&user_id=9&sname=test';
//    $sss = 'exch=exmo&fn=balance&user_id=2&sname=test';
//    $sss="exch=exmo&fn=neworder&from=btc&to=rub&user_id=2&sname=boriska&price=400000&side=sell&amount=0.001&ordertype=limit";
//    $sss="exch=exmo&fn=orderstatus&orderid=1927507813&user_id=16&sname=boriska";
//    $sss="exch=exmo&fn=orderstatus&orderid=1940709950&user_id=16&sname=boriska";
    parse_str($sss, $_GET);
}

$fn = $_GET['fn'];

/// сервисные вызовы
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
if ($fn == "getaccounts") {
    $db = getdb();
    $sql = <<<SQL
SELECT A.exch, min(A.account_id) as account_id,
       min(B.account_name) as account_name, min(B.group_name) as group_name
FROM `keys` A, accounts B
where B.id = A.account_id
group by account_id order by A.exch, A.account_id
SQL;
    $rz = $db->query($sql);
    $ks = $rz->fetch_all(MYSQLI_ASSOC);
    $data = je($ks);
    echo $data;
    exit;
}

//if (is_null($_GET['exch'])) {
//    exit;
//}
//
//if (is_null($_GET['fn'])) {
//    exit;
//}
//
//if (is_null($_GET['user_id'])) {
//    exit;
//}

if (is_null($_GET['sname'])) {
//    exit;
    $_GET['sname'] = 'unname';
}


//$sname=$_GET['sname'];
//$exch = $_GET['exch'];

///////////////////////////////////////////////////////////////////////////////
//работа с ключами
//if ($fn == "getkeys") {
//    $sql = "select * from dsx order by id_key, user";
//    $rz = $db->query($sql);
//    $ks = $rz->fetch_all(MYSQLI_ASSOC);
//    $data = json_encode($ks, JSON_UNESCAPED_UNICODE);
//    echo $data;
//    exit;
//}
//
//if ($fn == "deletekey") {
//    $user = $_GET['user'];
//    $key_id = $_GET['key_id'];
//    $sql = "delete from dsx where id_key = $key_id and  user = $user";
//    $rz = $db->query($sql);
//    exit;
//}
//
//if ($fn == "addkey") {
//    $key_id = $_GET['key_id'];
//    $dt = microtime(true);
//    $user = $_GET['user'];
//    $key = $_GET['key'];
//    $secret = $_GET['secret'];
//    $sql = "insert into dsx values ('$key_id', '$dt', '$user', '$key', '$secret')";
//    $rz = $db->query($sql);
//    exit;
//}
///////////////////////////////////////////////////////////////////////////////


try {
    $exchange = getExch();
    /// котировки
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ($fn == 'fetchticker') {
//        $s = microtime(true);
        $from = $_GET['from'];
        $to = $_GET['to'];
        $sym = strtoupper($from . '/' . $to);
        $rz = $exchange->fetchTicker($sym);
        $rz['success'] = 1;
//        $rz['est'] = microtime(true) - $s;
        $data = je($rz);
        echo($data);
    }


    /// баланс счета
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ($fn == 'balance') {
//        $s = microtime(true);
        $rz = $exchange->fetchBalance();
        $rz['success'] = 1;
//        $rz['est'] = microtime(true) - $s;
        $data = je($rz);
        echo($data);
    }

    /// стакан
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ($fn == 'orderbook') {
//        $s = microtime(true);
        $from = $_GET['from'];
        $to = $_GET['to'];
        $sym = strtoupper($from . '/' . $to);
        $limit = 50;
        $rz = $exchange->fetch_order_book($sym, $limit);
        $rz['success'] = 1;
//        $rz['est'] = microtime(true) - $s;
        $data = je($rz);
        echo($data);
        unset($data);
    }

    /// установка ордера
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ($fn == 'neworder') {
//        $s = microtime(true);
        $from = $_GET['from'];
        $to = $_GET['to'];
        $symbol = strtoupper($from . '/' . $to);

        $side = $_GET['side'];//Buy or Sell
        $ordertype = $_GET['ordertype'];//The order type: limit or market

        $amount = $_GET['amount']; // your amount
        $price = $_GET['price']; // your price
        // overrides
        $params = [];
        $order = $exchange->create_order($symbol, $ordertype, $side, $amount, $price/*, $params*/);
//        unset($order['info']);
        $order['success'] = 1;
//        $order['est'] = microtime(true) - $s;
        $data = je($order);
        echo($data);
    }

    /// отмена ордера
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ($fn == 'cancelorder') {
//        $s = microtime(true);
        $orderid = $_GET['orderid'];
        $from = $_GET['from'];
        $to = $_GET['to'];
        $symbol = strtoupper($from . '/' . $to);
        $order = $exchange->cancel_order($orderid, $symbol);
        $order['success'] = !is_null($order);
//        $order['est'] = microtime(true) - $s;
        $data = je($order);
        echo($data);
    }

    /// получить открытые ордера
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ($fn == 'openorders') {
//        $s = microtime(true);
        $from = $_GET['from'];
        $to = $_GET['to'];
        $symbol = strtoupper($from . '/' . $to);
        $order = $exchange->fetch_open_orders($symbol);
        $order['success'] = 1;
//        $order['est'] = microtime(true) - $s;
        $data = je($order);
        echo($data);
    }

    /// получить все ордера
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ($fn == 'trades') {
//        $s = microtime(true);
        $from = $_GET['from'];
        $to = $_GET['to'];
        $symbol = strtoupper($from . '/' . $to);
        $order = $exchange->fetch_my_trades($symbol);
//        $trades['trades']=($order);
//        $trades['success'] = 1;
//        $order['est'] = microtime(true) - $s;
        $data = je(array_values($order));
        echo($data);
//        unset($data);
    }

    /// состояние ордера
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ($fn == 'orderstatus') {
//        $s = microtime(true);
        $id = $_GET['orderid'];
//        $from = $_GET['from'];
//        $to = $_GET['to'];
//        $symbol = strtoupper($from . '/' . $to);
        $exch = strtoupper($_GET['exch']);

        $orderstat = $exchange->fetch_order($id,$exch);

        $orderstat['success'] = 1;
//        if (!$orderstat['trades'])
        {
//            $trades = $exchange->fetch_my_trades($symbol);
            $orderstat['trades'] = [];
        }
//        $orderstat['est'] = microtime(true) - $s;
        $data = je($orderstat);
        echo($data);
    }
    /// сделки по ордеру
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ($fn == 'ordertrades') {
//        $s = microtime(true);
        $id = $_GET['orderid'];
//        $from = $_GET['from'];
//        $to = $_GET['to'];
//        $symbol = strtoupper($from . '/' . $to);
        $exch = strtoupper($_GET['exch']);

        $orderstat = $exchange->fetch_order_trades($id);

        $orderstat['success'] = 1;
//        $orderstat['est'] = microtime(true) - $s;
        $data = je($orderstat);
        echo($data);
    }
}
catch (\ccxt\BadResponse $err) {
    $e['success'] = 0;
    $e['error'] = 114;
    $e['info'] = 'BadResponse';
    $data = je($e);
    echo($data);
}
catch (\ccxt\InsufficientFunds $err) {
    $e['success'] = 0;
    $e['error'] = 115;
    $e['info'] = 'InsufficientFunds';
    $data = je($e);
    echo($data);
}

catch (\ccxt\InvalidOrder $err) {
    $e['success'] = 0;
    $e['error'] = 117;
    if ($err instanceof \ccxt\OrderNotFound)
        $e['error'] = 1171;

    if ($err instanceof \ccxt\OrderNotCached)
        $e['error'] = 1172;

    if ($err instanceof \ccxt\CancelPending)
        $e['error'] = 1173;

    if ($err instanceof \ccxt\OrderImmediatelyFillable)
        $e['error'] = 1174;

    if ($err instanceof \ccxt\OrderNotFillable)
        $e['error'] = 1175;

    if ($err instanceof \ccxt\DuplicateOrderId)
        $e['error'] = 1176;

    $e['info'] = $err->getMessage();
    $data = je($e);
    echo($data);
} catch (\ccxt\ExchangeError $err) {
    $e['success'] = 0;
    $e['error'] = 11;
//    $s=explode(' ', $err->getMessage(), 2);
    $e['info'] = $err->getMessage();//json_decode($s[1]);
    $data = je($e);
    echo($data);
} catch (\ccxt\NetworkError $err) {
    $e['success'] = 0;
    $e['error'] = 12;
    if ($err instanceof \ccxt\DDoSProtection)
        $e['error'] = 121;
    if ($err instanceof \ccxt\ExchangeNotAvailable)
        $e['error'] = 122;
    if ($err instanceof \ccxt\InvalidNonce)
        $e['error'] = 123;
    if ($err instanceof \ccxt\RequestTimeout)
        $e['error'] = 124;

    $e['info'] = $err->getMessage();
    $data = je($e);
    echo($data);
} catch (\ccxt\BaseError $err) {
    $e['success'] = 0;
    $e['error'] = 1;
    $e['info'] = $err->getMessage();
    $data = je($e);
    echo($data);
} catch (Error $err) {
    $e['success'] = 0;
    $e['error'] = 2;
    $e['info'] = $err->getMessage();
    $data = je($e);
    echo($data);
}

//wlog($start, microtime(true), $exchange, $data);
/*
+ BaseError 1
|
+---+ ExchangeError 11
|   |
|   +---+ AuthenticationError 111
|   |   |
|   |   +---+ PermissionDenied 1111
|   |   |
|   |   +---+ AccountSuspended 1112
|   |
|   +---+ ArgumentsRequired 112
|   |
|   +---+ BadRequest 113
|   |
|   +---+ BadResponse 114
|   |   |
|   |   +---+ NullResponse 1141
|   |
|   +---+ InsufficientFunds 115
|   |
|   +---+ InvalidAddress 116
|   |   |
|   |   +---+ AddressPending 1161
|   |
|   +---+ InvalidOrder 117
|   |   |
|   |   +---+ OrderNotFound 1171
|   |   |
|   |   +---+ OrderNotCached 1172
|   |   |
|   |   +---+ CancelPending 1173
|   |   |
|   |   +---+ OrderImmediatelyFillable 1174
|   |   |
|   |   +---+ OrderNotFillable 1175
|   |   |
|   |   +---+ DuplicateOrderId 1176
|   |
|   +---+ NotSupported 118
|
+---+ NetworkError (recoverable) 12
    |
    +---+ DDoSProtection 121
    |
    +---+ ExchangeNotAvailable 122
    |
    +---+ InvalidNonce 123
    |
    +---+ RequestTimeout 124
*/