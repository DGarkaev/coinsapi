<?php
/**
 * Created by PhpStorm.
 * User: dgarkaev
 * Date: 27.01.19
 * Time: 23:44
 */

///////////////////////////////////////////////////////////////////////////////
class dsx extends \ccxt\dsx
{
    /**
     * dsx1 constructor.
     * @param array $options
     * @throws \ccxt\ExchangeError
     */
    public function __construct($options = array())
    {
        parent::__construct(array_merge(array('i' => 1), $options));
    }

    public function nonce()
    {
        return $this->milliseconds();
    }

    public function fetch_open_orders($symbol = null, $since = null, $limit = null, $params = array())
    {
        $order = parent::fetch_open_orders($symbol, $since, $limit, $params);
        $openorder = [];
        // dsx на fetch_open_order возвращает все ордера
        // поэтому выбираем из списка только те, которые открытые
        for ($i = 0; $i < count($order); $i++) {
            if ($order[$i]['status'] == 'open') {
                $order[$i]['trades'] = [];
                $openorder[] = $order[$i];
            }
        }
        return $openorder;
    }

    public function fetch_my_trades($symbol = null, $since = null, $limit = null, $params = array())
    {
        $trades = parent::fetch_my_trades($symbol, $since, $limit, $params);
        foreach ($trades as &$i) {
            $i['amount'] = $i['info']['volume'];
        }
        return $trades;
    }
}
