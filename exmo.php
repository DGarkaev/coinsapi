<?php
/**
 * Created by PhpStorm.
 * User: dgarkaev
 * Date: 27.01.19
 * Time: 23:41
 */


class exmo extends \ccxt\exmo
{
    public $nonceid;
    public $noncetime;
    protected $is_sync;
    protected $sem;

    public function nonce () {
        $this->nonceid = (int)(microtime(true)*1000);
        $this->noncetime = $this->nonceid/1000.0;
        return $this->nonceid;
    }

    /**
     * exmo constructor.
     * @param array $options
     * @throws \ccxt\ExchangeError
     */
    public function __construct($options = array())
    {
        parent::__construct(array_merge(array('i' => 1), $options));
        $this->sem = sem_get ($_GET['user_id']);
    }

    public function fetch_order($id, $symbol = null, $params = array())
    {
        $order = parent::fetch_order ($id, $symbol, $params);
//        HACK:берем статус ордера из списка всех ордеров по id
        $order = $this->fetch_orders();
        if ($order) {
            foreach ($order as $i) {
                if ($i['id'] == $id) {
                    $i['trades'] = [];
                    return $i;
                }
            }
        }
        return [];
    }



    public function fetch_balance($params = array())
    {
        $response = $this->privatePostUserInfo($params);
        $result = array('info' => $response);
        $currencies = array_keys($response['balances']);
        for ($i = 0; $i < count($response['balances']); $i++) {
            $currency = $currencies[$i];
            $account = $this->account();
            $account['free'] = floatval($response['balances'][$currency]);
            $account['used'] = floatval($response['reserved'][$currency]);
            $account['total'] = $this->sum($account['free'], $account['used']);
            $result[$currency] = $account;
        }
        return $this->parse_balance($result);
    }
    public function fetch_funding_fees ($params = array ()) {
        return null;
    }
    public function fetch_trading_fees ($params = array ()) {
        return null;
    }

    public function fetch_order_book ($symbol, $limit = null, $params = array ()) {
        $market = str_replace('/', '_',$symbol);//$this->market ($symbol);
        $request = array_merge (array (
            'pair' => $market,
        ), $params);
        if ($limit !== null)
            $request['limit'] = $limit;
        $response = $this->publicGetOrderBook ($request);
        $result = $response[$market];
        return $this->parse_order_book($result, null, 'bid', 'ask');
    }

    public function cancel_order ($id, $symbol = null, $params = array ()) {
//        $this->load_markets();
        $response = $this->privatePostOrderCancel (array ( 'order_id' => $id ));
        if (is_array ($this->orders) && array_key_exists ($id, $this->orders))
            $this->orders[$id]['status'] = 'canceled';
        return $response;
    }

    public function create_order ($symbol, $type, $side, $amount, $price = null, $params = array ()) {
//        $this->load_markets();
        $prefix = ($type === 'market') ? ($type . '_') : '';
//        $market = $this->market ($symbol);
        $market = str_replace('/', '_',strtoupper($symbol));
        if (($type === 'market') && ($price === null)) {
            $price = 0;
        }
        $request = array (
            'pair' => $market, //$market['id'],
            'quantity' => $amount,// $this->amount_to_precision($symbol, $amount),
            'type' => $prefix . $side,
            'price' => $price,// $this->price_to_precision($symbol, $price),
        );
        $response = $this->privatePostOrderCreate (array_merge ($request, $params));
        $id = $this->safe_string($response, 'order_id');
        $timestamp = $this->milliseconds ();
        $amount = floatval ($amount);
        $price = floatval ($price);
        $status = 'open';
        $order = array (
            'id' => $id,
            'timestamp' => $timestamp,
            'datetime' => $this->iso8601 ($timestamp),
            'lastTradeTimestamp' => null,
            'status' => $status,
            'symbol' => $symbol,
            'type' => $type,
            'side' => $side,
            'price' => $price,
            'cost' => $price * $amount,
            'amount' => $amount,
            'remaining' => $amount,
            'filled' => 0.0,
            'fee' => null,
            'trades' => null,
        );
        $this->orders[$id] = $order;
        return array_merge (array ( 'info' => $response ), $order);
    }

    public function fetch2 ($path, $api = 'public', $method = 'GET', $params = array (), $headers = null, $body = null) {
        sem_acquire($this->sem);
        $rz = parent::fetch2($path, $api, $method, $params, $headers, $body);
//        $request = $this->sign ($path, $api, $method, $params, $headers, $body);
//        $rz =  $this->fetch ($request['url'], $request['method'], $request['headers'], $request['body']);
        sem_release($this->sem);
        return $rz;
    }

}
