<?php
//Suppress E_DEPRECATED errors for statically calling a non-static method (this package is pretty old!)
//error_reporting(E_ALL & ~E_DEPRECATED);
require_once __DIR__ . '/vendor/autoload.php';
require_once 'db.php';

$limit=10;
if (!is_null($_GET['limit'])) {
    $limit=$_GET['limit'];
}

echo "<pre>"; #uncomment this line if running script in a browser

//The class isn't namespaced so just call it directly like so:
$tbl = new Console_Table();

$tbl->setHeaders(array('time', 'param', 'result'));

$db = getdb();
$sql = "select * from worklog_v limit $limit";
$rz = $db->query($sql);

foreach ($rz as $i) {
    $tbl->addRow(array($i['ndt'], $i['param'], $i['result']));
}

$db->close();

echo $tbl->getTable();