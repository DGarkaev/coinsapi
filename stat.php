<?php
require_once 'db.php';

header('Content-Type: application/json;charset=utf-8');
header('Access-Control-Allow-Origin: *');

if (!file_exists("coinsapi.db")) {
    echo json_encode('', JSON_UNESCAPED_UNICODE);
 exit();
}

$db = getdb();
$qps=1;//ограничение на количество запросов в секунду
$sql = <<<SQL
select * from (select  a.id_dsx, 
        a.dt, 
        a.odt, 
        ROUND($qps/(a.dt - a.odt)*100,0) as t 
        from dsxstat a order by id_dsx, dt desc) b
where b.t > 80
SQL;

$rz = $db->query($sql);
$data = [];
$data['staterr']=[];
while ($row = $rz->fetchArray(SQLITE3_ASSOC)) {
    //$data[$row['id_dsx']][$row['dt']] = $row['t'];
    $data['staterr'][$row['id_dsx']][]= array(
            'time' =>  date("Y-m-d H:i:s",
            $row['dt'] + 6*3600), 'value' => $row['t']);
}

$sql=<<<SQL
select max(dt) as time, count(*)/(max(dt)-min(dt))  as value 
            from dsxstat where dt > (select max(dt)-60 from dsxstat)
SQL;
$rz = $db->query($sql);
$row = $rz->fetchArray(SQLITE3_ASSOC);
$row['time']= date("Y-m-d H:i:s", $row['time'] + 6*3600);
$data['q2s'] = $row;//array($row['t'] => $row['q2s']);
echo json_encode($data, JSON_UNESCAPED_UNICODE);


