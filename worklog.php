<?php
date_default_timezone_set('UTC');

require_once 'db.php';
$_GET['sname']='boriska';
$_GET['limit']=10;

$limit=10;
if (!is_null($_GET['limit'])) {
    $limit=$_GET['limit'];
}

$sname = $_GET['sname'];

$sql="SELECT ndt, param, result, fn, sname FROM `worklog` WHERE sname = '$sname' order by ndt desc limit $limit";

$db = getdb();
$rz = $db->query($sql);
if (!$rz) exit;
$d = $rz->fetch_all(MYSQLI_ASSOC);
foreach ($d as &$item) {
    $item['date'] = $item['ndt'];
    unset($item['ndt']);
    $item['param'] = json_decode($item['param']);
    $item['result'] = json_decode($item['result']);
}


$data = json_encode($d,
    JSON_PRETTY_PRINT |
    JSON_UNESCAPED_SLASHES |
    JSON_NUMERIC_CHECK |
    JSON_UNESCAPED_UNICODE );
header('Content-Type: application/json;charset=utf-8');
echo ($data);