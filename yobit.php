<?php
/**
 * Created by PhpStorm.
 * User: dgarkaev
 * Date: 27.01.19
 * Time: 23:45
 */

///////////////////////////////////////////////////////////////////////////////
class yobit extends \ccxt\yobit
{
    /**
     * dsx1 constructor.
     * @param array $options
     * @throws \ccxt\ExchangeError
     */
    public $nonceid;
    public $noncetime;
    protected $db;
    protected $sem;

    function get_nonce()
    {
        $user_id = $_GET['user_id'];
        $this->db = getdb();
//        $this->db->begin_transaction();
        $sql = "select a.value_nonce from nonce as a where a.exch = 'yobit' and a.account_id = $user_id";// for update";
        $rz = $this->db->query($sql);
        $nonce = (int)$rz->fetch_array(MYSQLI_NUM)[0];
        $nonce++;
        $sql = "update nonce set value_nonce = $nonce where exch = 'yobit' and account_id = $user_id";
        $this->db->query($sql);
//        $this->db->commit();
        return $nonce;
    }

    public function __construct($options = array())
    {
        $param['enableRateLimit'] = true;
        parent::__construct(array_merge(array('i' => 1), $options));
//        $this->urls['api']['public'] = 'https://yobit.io/api';
//        $this->urls['api']['private'] = 'https://yobit.io/tapi';

        $this->sem = sem_get ($_GET['user_id']);
    }

    function __destruct() {
//        print "Уничтожается " . __CLASS__  . "\n";
//        $user_id = $_GET['user_id'];
//        $this->nonceid++;
//        $nonce = $this->nonceid;
//        $sql = "update nonce set value_nonce = $nonce where exch = 'yobit' and account_id = $user_id";
//        $rz = $this->db->query($sql);
//
//        $this->db->commit();
    }

    public function nonce()
    {
        $this->nonceid = $this->get_nonce('yobit');
        $this->noncetime = microtime(true);
        return $this->nonceid;
    }

    public function fetch_order_book($symbol, $limit = null, $params = array())
    {
        $market = explode('/', strtolower($symbol));//$this->market ($symbol);
        if ($market[0] == 'rub')
            $market[0] = 'rur';
        if ($market[1] == 'rub')
            $market[1] = 'rur';
        $market = $market[0] . '_' . $market[1];

        $request = array(
            'pair' => $market,
        );
        if ($limit !== null) {
            $request['limit'] = $limit; // default = 150, max = 2000
        }
        $response = $this->publicGetDepthPair(array_merge($request, $params));
        $orderbook = $response[$market];
        return $this->parse_order_book($orderbook);
    }

    public function fetch_my_trades ($symbol = null, $since = null, $limit = null, $params = array ()) {
        $rz = parent::fetch_my_trades($symbol, $since, $limit, $params);
        usort($rz, function ($o1, $o2){
            return $o1['timestamp'] - $o2['timestamp'];
        });
        return $rz;
    }

    public function fetch2 ($path, $api = 'public', $method = 'GET', $params = array (), $headers = null, $body = null) {
        sem_acquire($this->sem);
        $rz = parent::fetch2($path, $api, $method, $params, $headers, $body);
        sem_release($this->sem);
        return $rz;
    }


    }
